var expect = require("chai").expect;
var rewire = require("rewire");

var order = rewire("../lib/order");

describe("Ordering Items", function() {

    this.timeout(10000);
    var testData = [
		{sku: "AAA", qty: 10},
		{sku: "BBB", qty: 0},
		{sku: "CCC", qty: 3}
	];

	beforeEach(function() {
	       order.__set__("inventoryData", testData);
	});

	it("order an item when there are enough in stock", function(done) {
        var testInput = [
            {sku: "AAA", qty: 2},
            {sku: "CCC", qty: 3},
        ];

        var testOutput = [
            true,
            true
        ];

        var res = [];
        testInput.forEach(function(input, i){
            res.push(order.orderItem(input.sku, input.qty, function(){
                if (i == testInput.length - 1)
                    done();
            }));
        });
        expect(res).to.deep.equal(testOutput);
	});


    it("order an item when there are out of in stock", function() {
        var testInput = [
            {sku: "AAA", qty: 20},
            {sku: "BBB", qty: 10},
        ];

        var testOutput = [
            false,
            false
        ];

        var res = [];
        testInput.forEach(function(input, i){
            res.push(order.orderItem(input.sku, input.qty));
        });
        expect(res).to.deep.equal(testOutput);
	});

});
