var fs = require("fs");
var name = "";

process.stdout.write("Directory name: ");
process.stdin.on("data", function(data){
    name = data;
    process.stdin.pause();
});

process.stdin.on("pause", function(){
    if (fs.existsSync(name)) {
    	process.stdin.write("Directory already there\n");
    } else {
    	fs.mkdir(name, function(err) {

    		if (err) {
    			process.stdin.write(err + "\n");
    		} else {
    			process.stdin.write("Directory Created\n");
    		}
    	});

    }
});
