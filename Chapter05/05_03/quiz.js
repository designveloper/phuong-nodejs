var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);
var fs = require("fs");

var questions = [
    "1 + 2 = ? > ",
    "5 - 1 = ? > ",
    "3 * 9 = ? > ",
    "8 / 4 = ? > ",
    "2 - 3 = ? > "
];

var results = [
    3,
    4,
    27,
    2,
    -1
];

var answers = [];
var i = 0;
var count = 0;
rl.on("line", function(answer){
    answers.push(answer);
    let content = questions[i].slice(0, questions[i].length - 4) + answers[i] + "\n";
    fs.appendFileSync("quiz.txt", content);
    i++;
    if (i < questions.length) {
        rl.setPrompt(questions[i]);
        rl.prompt();
    } else {

        rl.close();
    }
});

rl.on("close", function(){
    for (let i = 0; i < questions.length; i++) {
        if (results[i] == answers[i]) {
            count++;
        }
    }
    fs.appendFileSync("quiz.txt", `\n\n==================\n\nYou score:  ${count}/${questions.length}`);
    console.log("Finish !")
    process.exit();
});

fs.writeFileSync("quiz.txt", `Quiz\n==================\n\n`)
rl.setPrompt(questions[0]);
rl.prompt();
