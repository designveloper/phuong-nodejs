# [Node.js Essential Training](https://www.lynda.com/Node-js-tutorials/Node-js-Essential-Training/417077-2.html)

## Chapter 01 - What is NodeJS ?

* NodeJS is single-threaded and asynchronous
* Events are raised and recorded in an event queue, and then handled in the order that thay were raised

## Chapter 02 - Installing NodeJS

* Can download from [nodejs.org](https://nodejs.org/)
* Can use commandline
    * `sudo apt-get install python-software-properties`
    * `curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -`
    * `sudo apt-get install nodejs`

## Chapter 03 - Node core

### The global object
* Some global object: `console`, `__dirname`, `__filename`
* Import a module: `require(module_name)`
* Run a nodejs file: `node <filename>`
* View more about [global object](https://nodejs.org/api/globals.html)

### Argument variables with `process.argv`
* `process.argv` store all the information from the terminal or command prompt as an array
* Example:
    * when we run this command: `node app.js add 5 6`
    * `process.argv = [<environment_path>, <file_path>, "add", "5", "6"]`

### Standard input and standard output
* `process.stdin` & `process.stdout`
* `process.stdout`
    * `process.stdout.write(...)` to write to the console
    * `process.stdout.clearLine(...)` to clear the last line in the console
    * `process.stdout.cursorTo(<pos>)` to move the cursor to the `pos` position
* `process.stdin.on('data', <call_back_func>)` to listen the input data event
* `process.on('exit', <call_back_func>)` to listen the exit event

### Global timing functions
* Use:
    * `setTimeout(<call_back_func>, time)`
    * `setInterval(<call_back_func>, time)`
    * `clearTimeout(<timeout_var>)`
    * `clearInterval(<interval_var>)`
    * `time` in milisecond

## Chapter 04 - Node modules

### Core modules
* Core modules are available to import without install from npm, they come with installaion of nodejs
* Some core modules: `path`, `util`, `v8`

### Collecting information with Readline
* Import module: `var readline = require("readline")`
* Create an instance of readline: `var rl = readline.createInterface(process.stdin, process.stdout)`
* `rl.question(<question_string>, call_back_func(answer){...} )`
* `rl.setPrompt(<content>)`: when `rl.prompt()` is called, the content will be write to output
* `rl.on("line", call_back_func)`: listen `line` event which is fired when user press `Enter` or `Return`
* `rl.on("close", call_back_func)`: listen `close` event when `rl.close()` is called

### Handling events with EventEmitter
* Import: `var EventEmitter = require('events').EventEmitter`
* Example code

```javascript
var emitter = new EventEmitter();
emitter.on("test", function(arg1, arg2,...){
    console.log("An event was fired");
});

emitter.emit("test", arg1, arg2,...);
```

* Can extend from `EventEmitter`

```javascript
class Person extends EventEmitter {
    constructor(name) {
        super();
        this.name = name;
        this.on("create", function(){
            console.log(`A person name "${this.name}" was created`);
        });
        this.emit("create");
    }
}

var john = new Person("John");
```

### Exporting custom modules
* In NodeJS, each file is a module
* To export module

```javascript
module.exports = <object>
```

### Creating child process with exec
* Import: `var exec = require('child_process').exec`
* Run command:

```javascript
exec(<command>, function(err, stdout) {
    if (err) {
        throw ...
    }

    console.log(stdout); //stdout contains the content which be written output if we execute the command
});
```
### Creating child process with spawn
* Import: `var spawn = require('child_process').spawn`
* Run command:`var command = spawn(<command>, [<arguments>])`
* If `exec` return the buffer, `spawn` will return the stream. We can write:

```javascript
command.stdout.on("data", function(data){...});
command.stdin.on("data", function(data){...});
command.stderr.on("data", function(data){...});
command.on("close", function(){...});
```

## Chapter 05 - The file system

Import module: `require("fs")`

### Listing directory files
* Synchronous

```javascript
var files = fs.readdirSync("<dir>");
console.log(files);
```

* Asynchronous

```javascript
fs.readdir("<dir>", function(err, files) {
    if (err) {
        throw err;
    } else {
        console.log(files);
    }
});
console.log("Reading directory...");

```

### Reading files
* Can read the content of both text and binary files, when reading a text file, we have to pass the encoding parameter, if not, it'll be read as a  binary file

```javascript
var content = fs.readFileSync("<filename>", "<encoding>");

fs.readFile("<filename>", "<encoding>", function(err, content) {...});
```

### Writing and appending files
```javascript
fs.writeFile("<filename>", "<content>", function(err) {...})

fs.writeFileSync("<filename>", "<content>")

fs.appendFile("<filename>", "<content>", function(err) {...})

fs.appendFileSync("<filename>", "<content>")
```

### Renaming and removing files
```javascript
// To rename
fs.rename("<old_name>", "<new_name>", function(err) {...});
fs.renameSync("<old_name>", "<new_name>");
//We can change parent directory in new name to move

// To remove
fs.unlink("<file_name>", function(err) {...});
fs.unlinkSync("<file_name>");
```

### Renaming and removing directories
* We use `rename` function to rename directories like files
* To remove directories, we use `rmdir`, but it can remove the empty directory. To remove a not-empty directory, we have to remove all file in it

### Readable file streams
```javascript
var stream = fs.createReadStream("<filename>", "<encoding>");
stream.on("data", function(chunk) {...});
stream.on("end", function(){...});
```
### Writable file streams
```javascript
var stream = fs.createWriteStream("<filename>");
stream.write("<content>");
stream.close();
```

## Chapter 06 - The HTTP module

### Making a request
```javascript
var https = require("https");
var options = {
	hostname: "...",
	port: 443,
	path: "/...",
	method: "GET"
};

//req is similar to a stream
var req = https.request(options, function(res) {
	res.on("data", function(chunk) {...});
	res.on("end", function() {...});

});

req.on("error", function(err) {...});
req.end();
```

### Building a web server
```javascript
var http = require("http");

http.createServer(function(req, res) {

	res.writeHead(200, {"Content-Type": "<type>"});

	res.end(...);

}).listen(3000);
```

* Content-Type can be: `text/plain`, `text/html`, `text/css`, `image/jpeg`, `text/json`, ...

### Serving files
* We can read from file, store data in a variable, and then send to `res` using `res.end(data)`
* `createReadStream("<file>").pipe(res)`

### Serving JSON file
```javascript
res.writeHead(200, {"Content-Type": "text/json"});
res.end(JSON.stringify(data));
```

### Collecting POST data
```javascript
http.createServer(function(req, res) {
	if (req.method === "POST") {
		req.on("data", function(chunk) {...});
		req.on("end", function() {...});
	}
}).listen(3000);
```

## Chapter 07 - Node Package Manager

### Installing npms locally
* `npm install <package_name>`
* `npm remove <package_name>`
* `npm ls`

### Installing npms globally
* `npm install -g <package_name>`
* `npm remove -g <package_name>`

## Chapter 08 - Web Servers

### The package.json file
* This file store information about the project and dependencies that have to be installed to run the project
* `npm install <package_name> --save`: to add the package to the dependencies, whenever we want to reinstall all the packages, we just use `npm install`
* `npm remove <package_name> --save`: to remove package from node_modules and dependencies

### Intro to Express
```javascript
var express = require("express");
var app = express();

//use a function as a middle-ware
//the function to use have to have three arguments like the following
app.use(function(req, res, next) {
    ...
    next();
});

//middle-ware for a static directory
app.use(express.static("./public"));


app.listen(3000);
```

### Express routing and CORS
* In the example in this videos, the dictionary-api can only serve to the request made from the same domain name. The request from the other domain cannot be servered, so we use CORS to solve this
* CORS: Cross Origin Resource Sharing
* We use: `app.use(cors())`

### Express post bodies and params
* `body-parser`is middle-ware that will help us parse the data from post request
* We use following middle-ware, the data from post request will be stored in req.body as a json object
```javascript
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
```
* If we have a request at route: `/.../:<param>`, we can use `req.params.param` to get data. Example:

```javascript
app.get("/:name", function(req, res){
    //log the name param
    console.log(req.params.name);
});

//If we GET: /John, the console will print 'John'
```

## Chapter 09 - Web Sockets

### Creating a WebSocket server & Broadcasting messages with WebSockets
* Install package: `npm install --save ws`
* In server

```javascript
//create web socket server
var WebSocketServer = require("ws").Server;
var wss = new WebSocketServer({ port: 3000 });

wss.on("connection", function(ws) {

    // to send data to current client
	ws.send(<data>);

    //on message event
    ws.on("message", function(message) {...});

    //broadcast to all clients
    wss.clients.forEach(function(client) {...});
});

```

* In client

```javascript
var ws = new WebSocket("ws://localhost:3000");

ws.onopen = function() {...};

ws.onclose = function() {...};

//payload.data contain the received data
ws.onmessage = function(payload) {...};

ws.send(data); //to send data to server
```

### Creating WebSockets with Socket.IO
* Install package: `npm install --save socket.io`. In addition, we have to install `express` and `http` package
* More detail in exercise...

## Chapter 10 - Testing and Debugging

### Testing with mocha and Chai
* Require: `mocha` and `chai` package

```javascript
var expect = require("chai").expect;
var tools = require("../lib/tools");

describe("<suite>", function() {
	it("<test_case_1>", function() {...});
    it("<test_case_2>", function() {...});
    ...
});
```

### Asynchronous mocha testing
```javascript
describe("<suite>", function() {

    //timeout for request
	this.timeout(5000);

	it("<test_case>", function(done) {
        ...
        done();
	});
});
```

### Mocking a server with Nock
* Require `nock` package

```javascript
nock("<domain>").get("/<route>").reply(<status>, "<data>");

//Example
nock("https://en.wikipedia.org")
    .get("/wiki/Abraham_Lincoln")
    .reply(200, "Mock Abraham Lincoln Page");
```
* Hook in mocha: `before(callback)`, `beforeEach(callback)`, `after(callback)`, `afterEach(callback)`

### Injecting dependencies with rewire
```javascript
var rewire = require("rewire");
var order = rewire("../lib/order");

//inject the mock data
order.__set__("<name>", mockData);
```

### Advanced testing sinon

### Code coverage with Istanbul
* Generate the code coverage report
* Install `istanbul` package
* Run it with mocha: `istanbul cover _mocha`

### Testing HTTP endpoints with Supertest
* Install `supertest` package
* To test http request

```javascript
var request = require("supertest");
var app = require(".../app");

request(app).get("/<route>").expect(<status_code>).end(done);
```

### Checking server responses with Cheerio
* Install `cheerio` package
* To check response with cheerio

```javascript
var cheerio = require("cheerio");

it("...", function(done) {
    request(app).get("/<route>").expect(200).end(function(err, res) {
        //we can get the DOM using '$' as jQuery
        var $ = cheerio.load(res.text);
        var element = $("<selecttor>");
        ...
        done();
    });
});
```

## Chapter 11 - Automation and Deployment

### Hinting your code with Grunt
* This section is about hinting javascript code. But if we use a powerful IDE or editor, we won't need to use it

### Converting LESS to CSS with Grunt
* This section is about converting LESS to CSS file, view more detail in exercise

### Bundling client scripts with Browserify
* This section is about bundling multiplle files in to one file, view more detail in exercise

### Rapid development with Grunt Watches
* This section is about watching the changes of file and automaticly generate file (LESS to CSS, bundling js files), view more detail in exercise

### Automation with npm

### Debugginh with npm
* Use `node-inspector`
