var EventEmitter = require("events").EventEmitter;

class Person extends EventEmitter {
    constructor(name) {
        super();
        this.name = name;
        this.on("create", function(){
            console.log(`A person name "${this.name}" was created`);
        });
        this.emit("create");
    }
}

var john = new Person("John");

var emitter = new EventEmitter();
emitter.on("test", function(){
    console.log("An event was fired");
});

emitter.emit("test");
