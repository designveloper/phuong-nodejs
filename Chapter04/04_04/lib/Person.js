var EventEmitter = require('events').EventEmitter;

class Person extends EventEmitter {
    constructor(name) {
        super();
        this.name = name;
        this.on("create", function(){
            console.log(`A person name "${this.name}" was created`);
        });
        this.emit("create");
    }
}

module.exports = Person;
