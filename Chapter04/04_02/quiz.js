var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

var questions = [
    "1 + 2 = ? > ",
    "5 - 1 = ? > ",
    "3 * 9 = ? > ",
    "8 / 4 = ? > ",
];

var results = [
    3,
    4,
    27,
    2
];

var answers = [];
var i = 0;
var count = 0;
rl.on("line", function(answer){
    answers.push(answer);
    i++;
    if (i < questions.length) {
        rl.setPrompt(questions[i]);
        rl.prompt();
    } else {
        rl.close();
    }
});

rl.on("close", function(){
    for (let i = 0; i < questions.length; i++) {
        if (results[i] == answers[i]) {
            count++;
        }
    }
    console.log(`You score ${count}/${questions.length}`);
    process.exit();
});

rl.setPrompt(questions[i]);
rl.prompt();
