if (process.argv.length != 5) {
    console.log("Syntax error !");
} else {
    var operation = process.argv[2];
    var first = parseInt(process.argv[3]);
    var second = parseInt(process.argv[4]);
    switch (operation) {
        case 'add':
            console.log(`${first} + ${second} = ${first + second}`);
            break;
        case 'sub':
            console.log(`${first} - ${second} = ${first - second}`);
            break;
        case 'mul':
            console.log(`${first} * ${second} = ${first * second}`);
            break;
        case 'div':
            if (second != 0)
                console.log(`${first} / ${second} = ${first / second}`);
            } else {
                console.log("Cannot div 0")
            }
            break
        default:
            console.log("Syntax error");
    }
}
