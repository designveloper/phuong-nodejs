var questions = [
    "5 + 1 = ?",
    "5 - 2 = ?",
    "2 * 8 = ?",
    "9 / 3 = ?",
    "8 - 15 = ?"
];
var trueAnswers = [
    6,
    3,
    16,
    3,
    -7
];
var answers = [];

function ask(i) {
    process.stdout.write(`\n ${questions[i]}  > `);
}

process.stdin.on('data', function(data) {

	answers.push(data.toString().trim());

	if (answers.length < questions.length) {
		ask(answers.length);
	} else {
		process.exit();
	}

});

process.on('exit', function() {

	process.stdout.write("\n");
    var count = 0;
    for (let i = 0; i < answers.length; i++) {
        if (answers[i] == trueAnswers[i]) {
            count++;
        }
    }
    process.stdout.write(`You score ${count}/5 points`)
	process.stdout.write("\n");

});

ask(0);
